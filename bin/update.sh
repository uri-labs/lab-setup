#!/bin/bash

#####################
#
# Static Resources are cloud resources that won't be modified during the study
# 
######################

#Directories - are the responsibility of the main script
#if the script has 'inc' in the middle, it will not set directories
#only set the LAB_DIR directory, then include core/


THIS_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
THIS_PARENT_DIR=$( dirname "${THIS_DIR}" )

# get functions 
source "${THIS_PARENT_DIR}/.bin/functions.inc.sh"
source "${THIS_PARENT_DIR}/.bin/update/main.inc.sh"