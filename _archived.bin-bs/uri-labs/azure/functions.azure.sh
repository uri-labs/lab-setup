#!/bin/bash

####
#
# Azure Wrappers
#
#######



function createProjectContainer(){
######################################
# Azure: Resource Group
# GCP: Project
######################################
: ${1?"Usage: $0 Missing Project Id, Usage: createProjectContainer project_id location"}
: ${2?"Usage: $0 Missing Location    Usage: createProjectContainer project_id location"}


local project_id="${1}"
local location="${2}"
echo "Creating Resource Group " "${project_id} .."
az group create --location ${location} --name "${project_id}" --output tsv  | grep 'error|exist'
az group update -n ${project_id} --set tags.study=${STUDY_TAG} --output tsv  | grep 'error|exist'




}

function gcpCreateProjectConfig(){
local config_name="${1}"
gcloud config configurations create "${config_name}"
setProject;
setRegion;
setZone;
}

function gcpGetProjectConfig(){
local config_name="${1}"

gcloud config configurations activate "${config_name}"

}

# init
function setCloudDefaults(){
    #Sets defaults for cloud commands
    setProject
    setRegion
    setZone
}


# select project
function setProject(){


az configure --defaults group="${PROJECT_ID}"

}


function createStorageAccount(){

: ${1?"Usage: $0 Usage: createStorageAccount STORAGE_ACCOUNT_NAME PROJECT_ID PROJECT_LOCATION"}
: ${2?"Usage: $0 Usage: createStorageAccount STORAGE_ACCOUNT_NAME PROJECT_ID PROJECT_LOCATION"}
: ${3?"Usage: $0 Usage: createStorageAccount STORAGE_ACCOUNT_NAME PROJECT_ID PROJECT_LOCATION"}
local storage_account_name="${1}"
local project_id="${2}"
local location="${3}"

creation_time=30;


    # https://docs.microsoft.com/en-us/cli/azure/storage/account?view=azure-cli-latest
    # creates a stroage account with standard GRs and v2 storage
    echo "Creating Storage Account ${storage_account_name} ..."
az storage account create -n "${storage_account_name}" -g "${project_id}" -l "${location}" --sku Standard_GRS --kind StorageV2   --output tsv  | grep 'error|exist'


sleep "${creation_time}";


}


function createStorageBucket(){
: ${1?"Usage: $0 Usage: createStorageBucket bucket_name project_id storage_account_name public"}
: ${2?"Usage: $0 Usage: createStorageBucket bucket_name project_id storage_account_name public"}
: ${3?"Usage: $0 Usage: createStorageBucket bucket_name project_id storage_account_name public"}
: ${4?"Usage: $0 Usage: createStorageBucket bucket_name project_id storage_account_name public"}



#createStorageBucket \
#'website1' `#bucket_name` \
#'Production2' `#project_id` \
#'prod2uri1018' `#storage_account_name`
#'public' `#access`




local bucket_name="${1}"
local project_id="${2}"
local storage_account_name="${3}"
local access="${4}"



if [ "${access}" = 'public' ]; then
access="--public-access container"
else
access="";
fi




local creation_time=30;
local storage_key=$(azGetStorageAccountKey "${storage_account_name}" "${project_id}")

echo "Creating Bucket ${bucket_name} ..."
az storage container create ${access} --name "${bucket_name}" --account-name  "${storage_account_name}" --account-key "${storage_key}"   --output tsv  | grep 'error|exist'




}


function azGetStorageAccountKey (){
: ${1?"Usage: $0 Usage: azGetStorageAccountKey storage_account project_id"}
: ${2?"Usage: $0 Usage: azGetStorageAccountKey storage_account project_id"}


local storage_account="${1}"
local project_id="${2}"

    echo "$(az storage account keys list --account-name "${storage_account}" -g "${project_id}" --query [0].value  --output tsv)"
}


function setRegion(){
  az configure --defaults location="${DEFAULT_REGION}"

}

function setZone(){
    #NA for Azure
    echo '';
}

function config(){

gcloud init

}





function loginCloudAccount () {

   az login --username "${CLOUD_ACCOUNT}"
}


# for backward compat
function login () {

    loginServiceAccount;
}


function loginAdminAccount(){

echo "Logging with Admin Account"  "${ADMIN_ACCOUNT}"
az login --username "${ADMIN_ACCOUNT}"

}


function loginServiceAccount(){


# To configure, this
# create a service principal for the lab account sp-uritest4a
# get the key and its app id
# assign sp-uritest4a to subscription/IAM role=contributor

if [ -n "${AZURE_APP_ID}" ]; then
   echo ""
else
   echo "Cannot login to service account. Configure a valid Azure Service Account in lab.conf and retry."
   exit;
fi
# See lab.conf for info on how to create service account

echo "Logging into Azure with Service Account" "${AZURE_APP_ID} ..."

az login --service-principal --username "${AZURE_APP_ID}" --password "${AZURE_APP_KEY}" --tenant "${AZURE_TENANT_ID}" > /dev/null

}





function azGetUser()(

     echo "getUser not defined for azure"
     
)

function azGetProject()(

    gcloud config get-value project
    
)

function azGetRegion()(

   echo "getRegion not defined for azure"
    
)

function azShowAccount(){

   az account show --output table

}


function azGetResourceNames(){
resource="${1}"
region="${2}"
echo -e "\n########### ${resource} ########### "
_getResourceNames   "${resource}" "${region}"

    
}

function _gcpGetResourceNames(){
resource="${1}"
region="${2}"


gcloud compute "${resource}" list --filter="region=(${region})" --format="value(selfLink.scope(name))"  | grep -v default
 
}




function azCreateVMInstance(){




local instance="${1}" #www1
local machine_type="${2}" #"f1-micro"
local zone="${3}" #"${INSTANCE_GROUP_1_ZONE}"

local project=$(gcloud config get-value project)
echo "Creating VM Instance ${instance} for project ${project} in zone ${zone} ..."

gcloud compute instances create "${instance}" \
    --image-project debian-cloud \
    --image-family debian-9 \
    --machine-type "${machine_type}"  \
    --zone  "${zone}" \
    --tags http-tag \
    --metadata startup-script="#! /bin/bash
      sudo apt-get update
      sudo apt-get install apache2 -y
      sudo service apache2 restart
      echo '<!doctype html><html><body><h1>${instance}</h1></body></html>' | tee /var/www/html/index.html
      EOF"

}




function azAddNewDisk(){
disk_name="${1}"
disk_type="${2}"
disk_size="${3}"
mount_point="${4}"
device_path="${5}"
instance_name="${6}"
zone="${7}"


# create
#https://cloud.google.com/compute/docs/disks/add-persistent-disk
gcloud compute disks create "${disk_name}" --size "${disk_size}" --type "${disk_type}"

# attach
gcloud compute instances attach-disk "${instance_name}" --disk "${disk_name}"

# mount
# https://cloud.google.com/compute/docs/disks/add-persistent-disk#formatting
gcloud compute ssh --zone "${zone}" "${instance_name}" -- "sudo mkfs.ext4 -m 0 -F -E lazy_itable_init=0,lazy_journal_init=0,discard ${device_path}"

# create mount point
gcloud compute ssh --zone "${zone}" "${instance_name}" --command "sudo mkdir -p ${mount_point}"


# mount 
#gcloud compute ssh --zone "${zone}" "${instance_name}" -- 'sudo mount -o discard,defaults ${device_path} ${mount_point}'
gcloud compute ssh --zone "${zone}" "${instance_name}" --command "sudo mount -o discard,defaults ${device_path} ${mount_point}"


}
function azFillUpDisk(){
disk_name="${1}"
directory="${2}"
file_name="${3}"
disk_usage="${4}"
instance_name="${5}"
zone="${6}"

local command="cd "${directory}";for((i=1;i<="${disk_usage}";i++)); do sudo fallocate -l 1G "${file_name}""\${i}".jpg; done"
gcloud compute ssh --zone "${zone}" "${instance_name}" --command "${command}"


}



function azResizeDisk(){
# https://cloud.google.com/sdk/gcloud/reference/compute/disks/resize

disk_name="${1}"
zone="${2}"
size="${3}"

gcloud compute disks resize "${disk_name}" --zone="${zone}" --size="${size}"GB 

}


function grantServiceAccountProjectOwnership(){

: ${1?"Usage: $0 Missing Project Id, Usage: makeProjectOwner PROJECT_ID"}
local project_id="${1}"

# requires the object id of the lab group
echo "Assigning Permissions to ${project_id} resource group"
az role assignment create --assignee-object-id "${AZURE_APP_OBJECT_ID}" --role Owner -g "${project_id}"




}



function createVMBasic(){
##########
#
# This will create a basic vm with nothing installed
#
########


: ${1?"Usage: $0 Missing Project Id, Usage: createVM vm_name vm_size project_id vm_password"}
: ${2?"Usage: $0 Missing Project Id, Usage: createVM vm_name vm_size project_id vm_password"}
: ${3?"Usage: $0 Missing Project Id, Usage: createVM vm_name vm_size project_id vm_password"}
: ${4?"Usage: $0 Missing Project Id, Usage: createVM vm_name vm_size project_id vm_password"}

local vm_name="${1}"
local vm_size="${2}"
local project_id="${3}"
local vm_username="${4}"
local vm_password="${5}"

########
# Usage 
#createVMBasic \
#'prod2-1' `#vm_name` \
#'Standard_B1s' `#vm_size` \
#"${resource_group}" `#project_id` \
#'uritest' `#vm_username` \
#'B1sProduction2' `#vm_password`


# for vm sizes, see  https://docs.microsoft.com/en-us/azure/virtual-machines/windows/sizes-general
######


echo "Creating Virtual Machine" "${vm_name} .."



az vm create -n "${vm_name}" -g "${project_id}" --image ubuntults --size "${vm_size}" --admin-username "${vm_username}" --admin-password "${vm_password}" --tags "study=${STUDY_TAG}" 






}


function azDeployTemplate () {
# To find templates, create resource / template / select from dropdown
# Ref: https://docs.microsoft.com/en-us/azure/azure-resource-manager/resource-group-template-deploy-cli


# use loadbalancer template saved to .templates and taken from here
# https://raw.githubusercontent.com/azure/azure-quickstart-templates/master/201-load-balancer-ipv6-create/azuredeploy.json
# see https://azure.microsoft.com/en-us/resources/templates/201-load-balancer-ipv6-create/ for more info 


# to override value see this example:
# https://docs.microsoft.com/en-us/cli/azure/group/deployment?view=azure-cli-latest#az-group-deployment-create
#az group deployment create -g MyResourceGroup --template-file azuredeploy.json \
#                           --parameters @params.json --parameters MyValue=This MyArray=@array.json#

: ${1?"Usage: $0 Usage: azDeployTemplate deployment_name azure_template_dir  resource_group parameter_override"}
: ${2?"Usage: $0 Usage: azDeployTemplate deployment_name azure_template_dir  resource_group parameter_override"}
: ${3?"Usage: $0 Usage: azDeployTemplate deployment_name azure_template_dir  resource_group parameter_override"}




local deployment_name="${1}";
local azure_template_dir="${2}";
local resource_group="${3}";


# Example usage
# create mongo-db
# azDeployTemplate \
#"prod3-loadbalancer" `#deployment_name` \
#"mongodb" `#template_directory_name` \
#"Production3" `#resource_group`

# I was never able to get overrides to work, so abandoned it and will just use separate template directory for each deployment.





if [ -n "${parameter_override}" ]; then
  echo '';

  #parameter_override="--parameters ${parameter_override}"
else
  parameter_override='';
fi

echo "Deploying Template ${azure_template_dir} to resource group ${resource_group}"



az group deployment create --resource-group "${resource_group}" --name "${deployment_name}" --template-file "${DIR}"/"${TEMPLATES_DIR}"/"${azure_template_dir}/template.json" --parameters @"${DIR}"/"${TEMPLATES_DIR}"/"${azure_template_dir}/parameters.json"


}


# Delete Project (Resource Group for Azure)
function deleteProject(){


: ${1?"Usage: $0 Usage: deleteProject group_name  [nowait]"}


# Example usage
# deleteProject \
#"Production2" `#group_name` \
#"nowait" `#nowait, optional empty otherwise`


local group_name="${1}";

if [ "${2}" = "nowait" ]; then
  local no_wait="--no-wait"
else 
local no_wait=""
fi

echo "Deleting Resource Group ${group_name} ..."

az group delete --name ${group_name} ${no_wait} --yes

}






function azGetAzureMapsApiAccountName(){

##########
#
#  Returns the first AzureMaps account name in a resource group
#
#
#########




: ${1?"Usage: $0 Usage: azGetAzureMapsApiAccountName resource_group "}






local resource_group="${1}"

echo $(az maps account list -g "${resource_group}" --query [0].name  --output tsv)





}





function azDeleteAzureMapsAPIAccount(){

##########
#
#  Deletes the first Azure Maps account found in a Resource Group
#  Example Usage: 
#
#########




: ${1?"Usage: $0 Usage: azDeleteAzureMapsAPIAccount resource_group "}






local resource_group="${1}"
echo "Searching for maps account name for resource group ${resource_group} ...."
local maps_account=$(azGetAzureMapsApiAccountName "${resource_group}")



if [ -n "${maps_account}" ]; then
    echo "Found maps account ${maps_account}..."
    echo "Deleting ${maps_account} in resource group ${resource_group} ..."


    az maps account delete --name "${maps_account}" -g "${resource_group}"
    echo "Maps account ${maps_account} deleted "
else
  echo "No maps account found, skipping maps account delete"
fi





}


function uploadFolderToBucket(){

: ${1?"Usage: $0 Usage: uploadFolderToBucket bucket_name storage_account_name  local_directory_path project_id"}
: ${2?"Usage: $0 Usage: uploadFolderToBucket bucket_name storage_account_name  local_directory_path project_id"}
: ${3?"Usage: $0 Usage: uploadFolderToBucket bucket_name storage_account_name  local_directory_path project_id"}
: ${4?"Usage: $0 Usage: uploadFolderToBucket bucket_name storage_account_name  local_directory_path project_id"}

#uploadFolderToBucket \
#'www1' `#bucket_name` \
#"${website_storage_account}" `#storage_account_name` \
#"${DIR}/uri-labs/resources/acme-website" `#local_directory_path` \
#${resource_group}" `#project_id`


local bucket_name="${1}"
local storage_account_name="${2}"
local local_directory_path="${3}"
local project_id="${4}"


local storage_key=$(azGetStorageAccountKey "${storage_account_name}" "${project_id}")

echo "Uploading to ${bucket_name} ..."


az storage blob upload-batch --account-name "${storage_account_name}" --account-key \'"${storage_key}"\'  --source  "${local_directory_path}" --destination "${bucket_name}" 

}





function getAllProjectNames(){
#returns a list of all resource group names
    az group list --query [].name --output tsv

}



function deleteAllProjectsExcept (){
# will delete all resource groups except those in the 'keep' list which is a list of names separated by a new line
# e.g:
#keep_list=$(echo -e "Frames-4-You-URI\nStaging\nProduction\nDevelopment\nMongoDB-URI")
#deleteGroups  "${keep_list}"

: ${1?"Usage: $0 deleteGroups keep_list"}

local keep_list="${1}"

all_names=$(getAllProjectNames)

delete_list=$(diffList "${keep_list}" "${all_names}")

if [ -z "$delete_list" ]
then
      echo "no resource groups to delete..."
else
      echo "${delete_list}"  | while read group_name; do  deleteProject "${group_name}" ; done
fi




}
