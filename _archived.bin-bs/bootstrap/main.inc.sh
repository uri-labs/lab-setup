#!/bin/bash
#################
#
# Script
# if .inc. file, assume all directory variables and main functions.inc.sh are already included
#################



# include functions
source "${SUPPORT_DIR}/bootstrap/functions.inc.sh"

#local config
source "${SUPPORT_DIR}/bootstrap/config.conf"

: ${1?"Usage: $0 lab_number"}



setupGitAccount "${1}"
setupSSHKeys;
setupBashProfile;

echo 'Installing Lab Update...'
installLabUpdate \
"${LAB_UPDATE_REPO}" `#repo uri`


echo "Lab Update is installed at " "${LAB_UPDATE_INSTALL_DIR}"/"${LAB_UPDATE_DIR_NAME}"

echo "Next step: configure and run lab-update to install the study files"