

function setupGitAccount(){

: ${1?"Usage: setupGitAccount lab_number"}


local lab_number="${1}"



local lab_git_username="uritest${lab_number}a@gmail.com"
local lab_git_name="URI Lab ${lab_number}"


echo "configuring git .."

git config --global user.email "${lab_git_username}"
git config --global user.name "${lab_git_name}"




}

function setupSSHKeys(){


mkdir -p  "${SSH_KEY_DIR}"


echo "setting up SSH Keys ..."

echo "To download the private key to the lab machine, you'll need to login using an Azure account with access to the key vault holding the key"
az login



# 
az account set --subscription "${AZ_SUBSCRIPTION}"


az keyvault secret show --name "${AZ_PRIVATE_KEY_SECRET_NAME}" --vault "${AZ_KEY_VAULT}" --query '[value]' --output tsv > "${SSH_KEY_DIR}"/id_rsa






echo "setting up SSH permissions..."
find $HOME/.ssh -type d -exec chmod 700 {} \;
find  $HOME/.ssh -type f -exec chmod 600 {} \;





}

function setupBashProfile(){



echo "updating $HOME/.bash_profile ..."
echo "Be sure to check .bash_profile for duplicate functions if you've run this script multiple times"

cat "${SUPPORT_DIR}"/bootstrap/bash-profile >> "${HOME}"/.bash_profile



}



function installLabUpdate(){

#Lab update is a package that allows updating labs with new lab files

: ${1?"Usage: installLabUpdate repo_uri repo_dir"}

local repo_uri="${1}"
local repo_dir="${2}"


eval `ssh-agent`;
ssh-add "${SSH_KEY_DIR}/id_rsa";

echo "Cloning Source Repo" "${repo_uri}"

cd "${LAB_UPDATE_INSTALL_DIR}"

git clone  "${repo_uri}" -oUserKnownHostsFile=/dev/null  1>/dev/null 

}