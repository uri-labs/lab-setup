#!/bin/bash

#####################
#
# wrapper
# 
######################

#Directories - are the responsibility of the main script
#if the script has 'inc' in the middle, it will not set directories
#only set the LAB_DIR directory, then include core/


THIS_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
THIS_PARENT_DIR=$( dirname "${THIS_DIR}" )



# get functions 
source "${THIS_PARENT_DIR}/functions.inc.sh"
source "${SUPPORT_DIR}/update/main.inc.sh"
