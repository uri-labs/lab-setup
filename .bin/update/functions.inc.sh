function doSomething (){

    echo ''
}


function getLatestLabFiles(){

eval `ssh-agent`;
ssh-add "${SSH_KEY}";

echo "Cloning Source Repo" "${GIT_REMOTE_REPO}"

cd "${REPO_TARGET_DIR}"

rm -rf "${REPO_TARGET_DIR}"/"${GIT_DIR_NAME}" 2>&1>/dev/null

git clone  "${GIT_REMOTE_REPO}" -oUserKnownHostsFile=/dev/null "${GIT_DIR_NAME}" 1>/dev/null 


echo "Creating Archive ..."





cd  "${REPO_TARGET_DIR}"/"${GIT_DIR_NAME}" 

git submodule update --init --recursive --remote

# use fresh copy of git-archive-all
#curl -LO https://raw.githubusercontent.com/meitar/git-archive-all.sh/master/git-archive-all.sh  2>/dev/null

# or copy over our version
cp "${SUPPORT_DIR}"/update/git-archive-all.sh "${REPO_TARGET_DIR}"/"${GIT_DIR_NAME}/"


chmod u+x ./git-archive-all.sh  #2>/dev/null
./git-archive-all.sh ./  2>/dev/null && rm ./git-archive-all.sh

echo "Expanding Archive to Target Directory ${TARGET_DIR} ..." 

mkdir "${TARGET_DIR}"  2>/dev/null

tar -xf *.tar -C "${TARGET_DIR}"  2>/dev/null

echo "Deployment Complete, updated files are in ${TARGET_DIR}"

}

function getRemoteConfiguration(){


if "${REMOTE_CONFIG}"; then
    echo " retrieving remote configuration from ${REMOTE_CONFIG_URL}"
  remote_config_test=$(wget  -qO- "${REMOTE_CONFIG_URL}")

fi


# overrides any local configuration
source  <(echo -e "${remote_config_test}")



}