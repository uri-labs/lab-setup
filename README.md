# Lab Setup

Installs and updates lab files. 


## Download and install

    mkdir lab-setup
    cd lab-setup
    
**if git is installed**

    cd /c/
    git clone https://bitbucket.org/uri-labs/lab-setup.git
    cd lab-setup
    bin/bootstrap.sh


**if git is not installed:**

    Go to https://bitbucket.com/uri-labs
    
    Click 'lab-setup' 
    Click 'Downloads'
    Click 'Download repository'

    Extract and rename directory to lab-setup and place it in c:\


    
## Update 

    cd /c/lab-setup/
    git pull 



## Usage - Overview

    bin/bootstrap.sh # installs private keys from an azure vault
    bin/update.sh #downloads latest lab files

See each of the respective sections below for detailed usage and configuration



### Bootstrap


#### Usage

**bootstrap**

    bin/bootstrap.sh 6
    where '6' is the lab number.


#### Pre-requisites

    1. Create an Azure Key Vault
    2. Upload your private key to the key vaul as a secret


    az login
    az account set --subscription "YOUR SUBSCRIPTION NAME"
    az keyvault secret set --vault-name 'YOUR_VAULT_NAME' -n 'YOUR_SECRET_NAME' -f 'YOUR_LOCAL_PATH_TO_PRIVATE_KEY'

#### Configure



create a copy of the configuration example

    cp .bin/bootstrap/config-example.conf .bin/bootstrap/config.conf

edit with your custom settings 

    nano .bin/bootstrap/config.conf



### Lab Files Update

Clones and copies a git repo as an archive to a directory.


It does this by:

1. cloning to  .bin/.cache
2. creating a tar archive
3. extracting the archive to a target directory 

The main benefits are:

* *Uses archive ( zip/tar files) so doesn't litter your lab directory with git files, which is required if your tutorial or lab needs to use git   preventing the actual repo from being polluted by the tutorial. 
* Minimizes/prevents 'Submodules Pain'. Should handle submodules without the pain of breakage

#### Usage

    bin/update.sh    

#### configure 

    nano .bin/update/config.conf

replace remote repo path and git directory name with your custom values. 

**configuration example**


    REMOTE_CONFIG=false; # true for update to pull configuration from remote url
    REMOTE_CONFIG_URL="https://storage.googleapis.com/uri-labs-config/lab4/config-remote.conf"

    GIT_REMOTE_REPO='git@bitbucket.org:devops-1118/lab-gcp-current.git' # the git repo uri
    GIT_DIR_NAME='lab' # directory name after a successful clone
    TARGET_DIR='/c/lab'; # the final destination
    SSH_KEY="${HOME}/.ssh/lab/bitbucket/id_rsa"  # ssh key location
    REPO_TARGET_DIR="${CACHE_DIR}" # default: "${CACHE_DIR}" , .bin/.cache. Containers local cache.



#### Remote Configure


* You can optionally configure the script to pull the latest configuration from a url

REMOTE_CONFIG=true;
REMOTE_CONFIG_URL="https://console.cloud.google.com/storage/browser/uri-labs-config/lab4/config.conf"


## ToDo / Known Issues





